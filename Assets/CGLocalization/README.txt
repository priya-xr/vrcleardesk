This package contains the example scenes and assets required to test the CGLocalization system in the Editor and other platforms.

To open the example scenes go to the 'CGLocalization > Examples > Scenes' folder.
Two scenes are included:
 1. Localization Example: contains a small example with a basic main menu and options panel.
 2. Localized Behaviours: contains an example showing the localized behaviours.
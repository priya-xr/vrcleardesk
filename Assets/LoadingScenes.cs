﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScenes : MonoBehaviour
{
    [SerializeField]
    private Image progressBar;

    void Start()
    {
        StartCoroutine(LoadSelectedScene(SceneLoading.SceneName));
    }

    IEnumerator LoadSelectedScene(string name)
    {
        AsyncOperation loadlevel = SceneManager.LoadSceneAsync(name);

        while (loadlevel.progress < 1)
        {
            progressBar.fillAmount = loadlevel.progress;
            yield return new WaitForEndOfFrame();

        }

        yield return null;
       
    }
}

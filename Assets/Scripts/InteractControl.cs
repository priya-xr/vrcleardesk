﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class InteractControl : MonoBehaviour
{
    // Start is called before the first frame update
   public void DisableInteraction()
    {
       // GetComponent<VRTK_InteractableObject>().enabled = false;
        GetComponent<VRTK_InteractableObject>().isGrabbable = false;
    }

    public void EnableInteraction()
    {
        GetComponent<VRTK_InteractableObject>().isGrabbable = true;

    }
}

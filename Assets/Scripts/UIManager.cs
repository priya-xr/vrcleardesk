﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public void LoadMod1()
    {

        SceneManager.LoadScene("Module1");
    }

    public void LoadMod2()
    {

        SceneManager.LoadScene("Module2");
    }

    public void LoadMod3()
    {

        SceneManager.LoadScene("Module3");
    }

    public void LoadEval()
    {

        SceneManager.LoadScene("Evaluation");
    }

    public void LoadOnboard()
    {

        SceneManager.LoadScene("Onboarding");
    }

    public void LoadStart()
    {
        SceneManager.LoadScene("Start");
    }

}

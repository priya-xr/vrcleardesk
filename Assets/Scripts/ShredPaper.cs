﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class ShredPaper : MonoBehaviour
{
    public Animator crumpledpaper;
    public Animator shredder;
    public GameObject doc;
    public GameObject roll;  

    public void unfold()
    {
        crumpledpaper.SetTrigger("Unfold");
    }

    public void StartShred()
    {
        roll.SetActive(false);
        doc.SetActive(true);
        //s.Show(doc);
        shredder.SetTrigger("shred");

        

    }

   public void PaperPicked()
    {
        GetComponent<MeshRenderer>().enabled = false;
        this.transform.Find("Crushed").GetComponent<MeshRenderer>().enabled = true;
    }
 
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class Content : ScriptableObject
{
    [TextArea]
    public string english;
    [TextArea]
    public string arabic;
}

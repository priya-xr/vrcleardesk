﻿using HighlightPlus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightManager : MonoBehaviour
{
    protected GameObject highlightedObject;
     
    // Start is called before the first frame update
    void Start()
    {
        
    }   

    public void TurnOff(string TagName)
    {
        highlightedObject = GameObject.FindWithTag(TagName);
        highlightedObject.GetComponent<HighlightEffect>().enabled = false;
    }

    public void TurnOn(string TagName)
    {
        highlightedObject = GameObject.FindWithTag(TagName);
        highlightedObject.GetComponent<HighlightEffect>().enabled = true;
    }

    public void HighlightON(GameObject obj)
    {
        // highlightedObject = obj;
        obj.GetComponent<HighlightEffect>().enabled = true;
    }

    public void HighlightOFF(GameObject obj)
    {
        // highlightedObject = obj;
        obj.GetComponent<HighlightEffect>().enabled = false;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}

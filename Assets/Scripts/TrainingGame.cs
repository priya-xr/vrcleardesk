﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TrainingGame : MonoBehaviour
{
    public Timer timer;
    public TrainingModule[] module = new TrainingModule[3];
    private int currentmoduleindex;
    public Text TT;

    public Text eID;
    public Text TotalTime;
    public Text mod1time;
    public Text mod2time;
    public Text mod3time;

    [HideInInspector]
    public static double time1;
    public static double time2;
    public static double time3;

    void Start()
    {
        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "Module1")
            currentmoduleindex = 0;
        else if (scene.name == "Module2")
            currentmoduleindex = 1;
        else if (scene.name == "Module3")
            currentmoduleindex = 2;

        // timer = GetComponent<Timer>();
        Debug.Log(currentmoduleindex);
    }

    public void StartMyTimer()
    {
        timer.StartTimer(9000000000);
    }

    public void StoreTime()
    {
        Debug.Log(timer.CurrentTime);
        //module[currentmoduleindex].timeTaken = timer.CurrentTime;
        switch(currentmoduleindex)
        {
            case 0:
                time1 = timer.CurrentTime;
                break;


            case 1:
                time2 = timer.CurrentTime;
                break;

            case 2:
                time3 = timer.CurrentTime;
                break;
        }
        
       // TT.text = timer.CurrentTime.ToString();
        Debug.Log(module[currentmoduleindex].timeTaken);
        timer.StopTimer();
    }

    public void DisplayScore()
    {
        TrainingScore trainingScore = GetScore();
        //sessionManager.SetCurrentTrainingDetails(trainingScore);

        object[] args = new object[5];
        // args[0] = sessionManager.CurrentSession.employeeID.ToString();
        eID.text = MyKeyboard.empid;
         TotalTime.text = trainingScore.totalTimeTaken;
        mod1time.text = trainingScore.module1TimeTaken;
        mod2time.text = trainingScore.module2TimeTaken;
        mod3time.text = trainingScore.module3TimeTaken;

       // trainingGameUI.UpdatePanelDetails("TrainingScorePanel", args);
        // trainingGameUI.ShowPanel("TrainingScorePanel", true);
    }


    private TrainingScore GetScore()
    {
        double m1TimeTaken = time1; // module[0].timeTaken;
        double m2TimeTaken = time2; //module[1].timeTaken;
        double m3TimeTaken = time3; //module[2].timeTaken;

        double totalTimeTaken = m1TimeTaken + m2TimeTaken + m3TimeTaken;

        string[] values = new string[5];

      //values[0] = sessionManager.CurrentSession.employeeID.ToString();
        values[1] = SecsToMinutes(totalTimeTaken);
        values[2] = SecsToMinutes(m1TimeTaken);
        values[3] = SecsToMinutes(m2TimeTaken);
        values[4] = SecsToMinutes(m3TimeTaken);

        TrainingScore trainingScore = new TrainingScore(values[1], values[2], values[3], values[4],
                                                        m1TimeTaken.ToString(), m2TimeTaken.ToString(), m3TimeTaken.ToString());

        return trainingScore;
    }


    private string SecsToMinutes(double seconds)
    {
        int min = Mathf.FloorToInt((float)seconds / 60f);
        int sec = Mathf.FloorToInt((float)seconds % 60f);

        return min.ToString("00") + "min " + sec.ToString("00") + "sec";
    }


    void Update()
    {


        if (Input.GetKeyDown("t"))
        {
            //StoreTime();
            //DisplayScore();
            Debug.Log("time1:" + time1);
            Debug.Log("time2:" + time2);
            Debug.Log("time3:" + time3);
            //Debug.Log(timer.GetComponent<Timer>().CurrentTime.ToString());
          //  Debug.Log(timer.CurrentTime.ToString());
        }
    }



    [System.Serializable]
    public class TrainingModule
    {
        
        public int ModID;
        public double timeTaken;
        public TrainingModule() { }
    }

}

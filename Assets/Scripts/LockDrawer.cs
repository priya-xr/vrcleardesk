﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockDrawer : MonoBehaviour
{
    private bool top = false;
    private bool middle = false;
    private bool filesnapped = false;
    public bool docsnapped = false;
    public GameObject TaskOver;
    public GameObject NextTask;
    public GameObject DrawClose;
    public GameObject PickKeys;
    public GameObject DrawClose0;
    public GameObject TaskOver0;

   public void TopLocked()
    {
        top = true;
        DrawerLocked();
    }

    public void MiddleLocked()
    {
        middle = true;
        DrawerLocked();
    }

    public void SnapFile()
    {
        filesnapped = true;
    }

    public void SnapDoc()
    {
        docsnapped = true;
    }

    public void CloseDrawer()
    {
        if(filesnapped)
        {
            DrawClose.SetActive(false);
            PickKeys.SetActive(true);
        }

        if(docsnapped)
        {
            DrawClose0.SetActive(false);
            TaskOver0.SetActive(true);
        }

    }
    private void DrawerLocked()
    {
        if(top == true && middle == true)
        {
            TaskOver.SetActive(false);
            NextTask.SetActive(true);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyKeyboard : MonoBehaviour
{
    public InputField input;

    [HideInInspector]
    public static string empid;


    public void ClickKey(string character)
    {
        input.text += character;
        empid = input.text;
        //input.text = input.text + character;
    }

    public void Backspace()
    {
        if (input.text.Length > 0)
        {
            input.text = input.text.Substring(0, input.text.Length - 1);
        }
    }

    public void Enter()
    {
        //VRTK_Logger.Info("You've typed [" + input.text + "]");
        Debug.Log(input.text);
        input.text = "";
    }

    void Start()
    {
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class RandomiseObjects : MonoBehaviour
{

    public TaskToGenerate[] alltasks;
    private TaskToGenerate task;
    public List<TaskToGenerate> ActiveTasks = new List<TaskToGenerate>();
    public TaskToGenerate keytask;

    public Text eID;
    public Text tscore;
    public Text mod1;
    public Text mod2;
    public Text mod3;


    void Start()
    {

        while (ActiveTasks.Count < 6)
        {
            task = alltasks[UnityEngine.Random.Range(0, alltasks.Length)];

            if(!ActiveTasks.Contains(task))
                 ActiveTasks.Add(task);

        }

        for (int j = 0; j < ActiveTasks.Count; j++)
        {
            ActiveTasks[j].TaskObject.SetActive(true);
        }

        ActiveTasks.Add(keytask);
      
    }

    void Update()
    {
        if (Input.GetKeyDown("n"))
        {
            DisplayScore();
        }

        if(KeyLockTrigger.DrawsLocked == true)
        {
            FinishKey();
        }
        /*
        if (Input.GetKeyDown("1"))
        {
            GetModuleScore(1);
        }

        if (Input.GetKeyDown("2"))
        {
            GetModuleScore(2);
        }

        if (Input.GetKeyDown("3"))
        {
            GetModuleScore(3);
        }*/

    }

    public void DisplayScore()//object sender, System.EventArgs args)
    {
        //timer.StopTimer();
        //evaluateGameUI.ShowPanel("ScorePanel", true);

        //0-employee id
        //1-score
        //2-module 1 score
        //3-module 2 score
        //4-module 3 score

        // string employeeID = sessionManager.CurrentSession.employeeID.ToString();
        string employeeID = MyKeyboard.empid;
        string totalScore = "";
        string m1Score = "";
        string m2Score = "";
        string m3Score = "";

        GetScore(out totalScore, out m1Score, out m2Score, out m3Score);

        eID.text = employeeID;
        tscore.text = totalScore;
        mod1.text = m1Score;
        mod2.text = m2Score;
        mod3.text = m3Score;
      //  o[0] = employeeID;
       // o[1] = totalScore;
       // o[2] = m1Score;
       // o[3] = m2Score;
       // o[4] = m3Score;

       // evaluateGameUI.UpdatePanelDetails("ScorePanel", o);

       // EvaluationScore evaluationScore = new EvaluationScore(totalScore, m1Score, m2Score, m3Score);
      //  sessionManager.SetCurrentEvaluationDetails(evaluationScore);
    }


    private void GetScore(out string totalScore, out string m1Score, out string m2Score, out string m3Score)
    {
        totalScore = "0;";
        m1Score = "0;";
        m2Score = "0;";
        m3Score = "0;";


        int totalFinishedTasks = 0;
        for (int i = 0; i < ActiveTasks.Count; i++)
        {
            if (ActiveTasks[i].Completed == true)
                totalFinishedTasks++;
        }

        totalScore = (((float)totalFinishedTasks / (float)ActiveTasks.Count) * 100f).ToString("F0") + "/100";

        Debug.Log(totalScore);

        for (int i = 0; i < ActiveTasks.Count; i++)
        {
            if (ActiveTasks[i].Completed == true)
                totalFinishedTasks++;
        }

        GetModuleScore(1, out m1Score);
        GetModuleScore(2, out m2Score);
        GetModuleScore(3, out m3Score);

    }

    private void GetModuleScore(int moduleIndex, out string score)
    {
        score = "-";
        int totalModuleTasks = 0;
        int completedTasks = 0;

        for (int i = 0; i < ActiveTasks.Count; i++)
        {
            if (moduleIndex == ActiveTasks[i].ModuleIndex)
            {
                totalModuleTasks++;
                if (ActiveTasks[i].Completed)
                {
                    completedTasks++;
                }
            }
        }
        if (totalModuleTasks == 0)
        {
            score = "-";
            return;
        }

        score = (((float)completedTasks / (float)totalModuleTasks) * 100f).ToString("F0") + "%";
        Debug.Log("Module" + moduleIndex + " " + score);
    }


    public void Finish(int i)
    {
        alltasks[i].TaskCompleted();
    }

    public void FinishKey()
    {
        keytask.TaskCompleted();
    }

    [System.Serializable]
    public class TaskToGenerate
    {
        public int TaskIndex;
        public int ModuleIndex;
        public GameObject TaskObject;
        public bool Completed;

        public void TaskCompleted()
        {
            this.Completed = true;
        }

    }

}

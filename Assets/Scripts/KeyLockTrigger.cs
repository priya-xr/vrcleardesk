﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using VRTK.Examples;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class KeyLockTrigger : MonoBehaviour
{
     // = new RandomiseObjects();
    public Animator lock1;
    public Animator lock2;
    public GameObject KeySet1;
    public GameObject KeySet2;
    public GameObject actualkeys;
   // private LockDrawer LD = new LockDrawer();
    public GameObject TopDrawer;
    public GameObject MiddleDrawer;
    private bool top = false;
    private bool middle = false;
    public GameObject TaskOver;
    public GameObject NextTask;
    public Text t;

    [HideInInspector]
    public static bool DrawsLocked = false;


    public void lockdraw()
    {
        if (FindKeyPos.drawname == "top")
        {
            KeySet1.SetActive(true);
            //KeySet1.GetComponentInParent<VRTK_InteractableObject>().isGrabbable = true;
            lock1.SetTrigger("lock");
            TopDrawer.GetComponent<VRTK_InteractableObject>().isGrabbable = false;
            top = true;
            DrawerLocked();
        }
        else if (FindKeyPos.drawname == "middle")
        {
            KeySet2.SetActive(true);
            lock2.SetTrigger("lock");
            MiddleDrawer.GetComponent<VRTK_InteractableObject>().isGrabbable = false;
            middle = true;
            DrawerLocked();
        }

       

    }

    private void DrawerLocked()
    {
        if (SceneManager.GetActiveScene().name == "Module3")
        {
            if (top == true && middle == true)
            {
               
                TaskOver.SetActive(false);
                NextTask.SetActive(true);
                t.text = "";

               }
        }

        if (top == true && middle == true)
        {

            DrawsLocked = true;
        }

    }

    public void DrawLocked()
    {
        KeySet1.SetActive(false);
        KeySet2.SetActive(false);
        actualkeys.SetActive(true);   
    }

}

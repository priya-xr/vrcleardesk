﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoading : MonoBehaviour
{   
   // [SerializeField]
   // private Image progressBar;
    public GameObject loader;

    [HideInInspector]
    public static string SceneName;

    public void LoadingScene(string scenename)
    {
        loader.SetActive(true);
        SceneName = scenename;
        StartCoroutine(LoadAsyncOperation());
    }

   
    IEnumerator LoadAsyncOperation()
    {
        AsyncOperation gameLevel = SceneManager.LoadSceneAsync(SceneName);

        while(gameLevel.progress < 1)
        {
          //  progressBar.fillAmount = gameLevel.progress;
            yield return new WaitForEndOfFrame();

        }
    }
}




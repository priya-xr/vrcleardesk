﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Translate : MonoBehaviour
{   
    [SerializeField]
    public static bool asArabic;

    public Font arabicFont;
    public bool arabicGameObject;
    public bool englishGameObject;

    // Start is called before the first frame update
    public void Start()
    {
        var thisText = GetComponent<UnityEngine.UI.Text>();
        if (thisText != null)
        {
            thisText.text = Translate.DisplayText(thisText.text);
            if (asArabic)
                thisText.font = arabicFont;
        }
       /* if (englishGameObject)
        {
            gameObject.SetActive(!asArabic);
        }
        else if (arabicGameObject)
        {
            gameObject.SetActive(asArabic);
        }*/

    }

    private void UpdateText()
    {
        var thisText = GetComponent<UnityEngine.UI.Text>();
        if (thisText != null)
        {
            thisText.text = Translate.DisplayText(thisText.text);
            if (asArabic)
                thisText.font = arabicFont;
        }
      /*  if (englishGameObject)
        {
            gameObject.SetActive(!asArabic);
        }
        else if (arabicGameObject)
        {
            gameObject.SetActive(asArabic);
        }*/
    }


    public void changetoArabic()
    {
        asArabic = true;
        // Start();
        UpdateText();
    }

    public void changetoEnglish()
    {
        asArabic = false;
        UpdateText();
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public static string DisplayText(string sourceText)
    {
        if (asArabic == false || sourceText == string.Empty)
        {
            Debug.Log("im emoty");
            return sourceText;
        }
        else
        {
            string returnValue;
            switch (sourceText)
            {
                case "SELECT LANGUAGE":
                    returnValue = "select lang";
                    break;
                //case "Purchase":
                //  returnValue = "购买";
                //break;
                case "ENGLISH":
                    returnValue = "ENGLISH";
                    break;
                case "*.7I":
                    returnValue = "*.7I";
                    break;
                case "ENTER EMPLOYEE ID":
                    returnValue = "麦格根酒庄的新成员 ";
                    break;
                case "Incorrect Employee ID. Re-Enter a valid one.":
                    returnValue = "Incorrect Employee ID. Re-Enter a valid one.";
                    break;
                case "Employee ID":
                    returnValue = "麦格根酒庄的新成员—— ";
                    break;
                case "SELECT MODE":
                    returnValue = "麦格根酒庄的新成员—— ";
                    break;
                case "Onboarding":
                    returnValue = "麦格根酒庄的新成员—— ";
                    break;
                case "In this module, you will learn about the basic interactions involved in this app.":
                    returnValue = "In this module, you will learn about the basic interactions involved in this app.";
                    break;
                case "Training":
                    returnValue = "Training";
                    break;
                case "In this module, you will learn about the Louvre's Clear Desk Policy, its importance and guidelines. You will later be tested on the same.":
                    returnValue = "In this module, you will learn about the Louvre's Clear Desk Policy, its importance and guidelines. You will later be tested on the same.";
                    break;
                case "Evaluation":
                    returnValue = "Evaluation";
                    break;
                case "In this module, you will be evaluated on your understanding of the 'clear desk policy', and its guidelines. You will need to remember everything from your training.":
                    returnValue = "In this module, you will be evaluated on your understanding of the 'clear desk policy', and its guidelines. You will need to remember everything from your training.";
                    break;
                case "WRITE YOUR MESSAGE":
                    returnValue = "请输入信息";
                    break;
                case "Founder's Gift":
                    returnValue = "庄主献礼";
                    break;
                case "Continue to purchase bottle?":
                    returnValue = "继续购买?";
                    break;
                case "The new luxury addition to the McGuigan Wines portfolio, the Founder’s Gift, comes to life through the magic of augmented reality.":
                    returnValue = "麦格根酒庄的新成员——“庄主献礼”，即将通过魔法般的 AR 增强现实技术，活灵活现地呈现眼前。";
                    break;
                case "Bring the lion to life! The new luxury addition to the McGuigan Wines portfolio, the Founder’s Gift, comes to life through the magic of augmented reality.":
                    returnValue = "麦格根酒庄的新成员—— “庄主献礼”，即将通过 AR 增强现实技术，活灵活现地呈现眼前";
                    break;
                case "Install the Founder's Gift app to gift your own bottle":
                    returnValue = "安装麦格根APP来赠送\r礼物";
                    break;
                case "Shiraz\n2016":
                    returnValue = "西拉\n2016";
                    break;
                case "Cabernet Sauvignon\n2016":
                    returnValue = "赤霞珠\n2016";
                    break;
                case "This wine was sourced from Langhorne Creek in South Australia where conditions are perfect for Cabernet Sauvignon. A fantastic vintage, the 2016 growing season was very warm, with long sunny days and cool nights ensuring ideal fruit ripeness. Matured in French oak for 18 months, this full-bodied red wine features true varietal expression, firm tannins and a long lingering finish. It pairs well with any red meat, especially beef steak.":
                    returnValue = "这是一款来自于南澳大利亚，兰好乐溪产区的高品质赤霞珠葡萄酒，同时该地区在2016年也是丰收的一年。这款酒在法国橡木桶和美国橡木桶中进行陈酿长达18个月。其香味浓郁，口感丰满，厚重，并有着坚实的单宁，回味悠长。和牛排搭配是一个不错的选择。";
                    break;
                case "The majority of this wine was sourced from Barossa Valley, a region famous for its Shiraz. A fantastic vintage, the 2016 growing season was very warm, with long sunny days and cool nights ensuring ideal fruit ripeness. Matured in French and American oak for 12 months, the wine is expertly blended to deliver a Shiraz with intense fruit depth and beautiful natural acidity. Perfect with a rare steak or roast chicken.":
                    returnValue = "这是一款经典的来自于南澳大利亚，巴罗萨产区的西拉葡萄酒，同时该地区在2016年也是丰收的一年。这款酒在法国橡木桶和美国橡木桶中进行陈酿长达12个月。后呈现出黑莓，香草等气息，其口感饱满，柔顺，且层次感强。可以和牛肉，羊肉，猪肉或BBQ等搭配。";
                    break;
                case "Region:":
                    returnValue = "区域: ";
                    break;
                case "Alcohol Level:":
                    returnValue = "酒精度:";
                    break;
                case "Acidity:":
                    returnValue = "酸度:";
                    break;
                case "Residual Sugar:":
                    returnValue = "残糖:";
                    break;
                case "Oak:":
                    returnValue = "橡木: ";
                    break;
                case "Winemaker:":
                    returnValue = "酿酒师:";
                    break;
                case "South Australia":
                    returnValue = "南澳大利亚";
                    break;
                case "French Oak":
                    returnValue = "法国橡木桶";
                    break;

                case "Privacy Policy":
                    returnValue = "隐私政策";
                    break;

                case "English":
                    returnValue = "English";
                    break;

                case "中文":
                    returnValue = "中文";
                    break;
                case "Check out this App from McGuigan Wines, hope you like the virtual gift I made you! Download the App for the full AR experience and see the lion come to life! https://www.mcguiganwines.com.au/qrc/founders_gift":
                    returnValue = "快来体验此结合了增强现实功能的app！活灵活现的金狮会带你进入全新的麦格根世界！https://www.mcguiganwines.com.au/qrc/founders_gift";
                    break;
                case "Sorry, you must be 18 or over to access this application.":
                    returnValue = "抱歉，您必须年满18岁才能访问此应用程序。";
                    break;
                case "Please Enter Your Year of Birth":
                    returnValue = "请输入您的出生年";
                    break;
                case "Please Enter Valid Birth Year":
                    returnValue = "请输入有效的出生年份";
                    break;

                default:
                    returnValue = "Missing Translation: " + sourceText;
                    break;
            }
            return returnValue;
        }
    }

}
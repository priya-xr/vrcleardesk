﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LockLaptop : MonoBehaviour
{
    // Start is called before the first frame update
    private bool WKey = false;
    private bool LKey = false;
    public GameObject HomeScreen;
    public GameObject LockScreen;
    public GameObject TaskFinish;
    public GameObject NextTask;
    public GameObject randomize;

    public void WButtonPressed()
    {
        WKey = true;
        ScreenLock();
    }

    public void LButtonPressed()
    {
        LKey = true;
        ScreenLock();
    }

    private void ScreenLock()
    {
        if (WKey == true && LKey == true)
        {
            HomeScreen.SetActive(false);
            LockScreen.SetActive(true);

            Scene scene = SceneManager.GetActiveScene();
            if (scene.name == "Module1")
            {
                TaskFinish.SetActive(false);
                NextTask.SetActive(true);
            }
            else
                randomize.GetComponent<RandomiseObjects>().Finish(1);
            
        }
            
    }

    public void CloseLaptop()
    {
        Debug.Log("Laptop Closed");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AnimTrigger : MonoBehaviour
{
    public static Animator anim;
    public static string param;

    public void StartTrigger(string p)
    {
        anim = GetComponent<Animator>();
        param = p;
    }

} 

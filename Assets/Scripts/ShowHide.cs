﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowHide : MonoBehaviour
{
    
    public void Show(string name)
    {
        GameObject obj = GameObject.FindGameObjectWithTag(name);
        obj.SetActive(true);
    }

    public void Hide(string name)
    {
        GameObject obj = GameObject.FindGameObjectWithTag(name);
        obj.SetActive(false);
    }

    public void ShowObject(GameObject obj)
    {
        obj.SetActive(true);
    }

    public void HideObject(GameObject obj)
    {
        obj.SetActive(false);
    }
}


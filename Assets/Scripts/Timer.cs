﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Timer : MonoBehaviour
{
    private bool canUpdate;
    private bool isTimerPaused;
    private double targetTime;
    private double currentTime;

    public EventHandler<double> OnTimerUpdated;
    public EventHandler OnTimerFinished;
    public EventHandler OnTimerPaused;
    public EventHandler OnTimerResumed;
    public EventHandler OnTimerStopped;

    public double CurrentTime { get => currentTime; }

    void Update()
    {
        if (!canUpdate)
            return;

        currentTime += Time.deltaTime;

        if (CurrentTime >= targetTime)
        {
            OnTimerFinished?.Invoke(this, new EventArgs());
            StopTimer();
        }
        else
        {
            OnTimerUpdated?.Invoke(this, CurrentTime);
        }



    }

    public void StartTimer(float targetTime)
    {
        this.targetTime = targetTime;
        canUpdate = true;
    }

    public void StopTimer()
    {
        this.targetTime = 0;
        canUpdate = false;
        currentTime = 0;
        OnTimerStopped?.Invoke(this, new EventArgs());

    }

    public void PauseTimer()
    {
        canUpdate = false;
        isTimerPaused = true;

        OnTimerPaused?.Invoke(this, new EventArgs());
    }

    public void ResumeTimer()
    {
        if (!isTimerPaused)
            return;

        canUpdate = true;
        OnTimerResumed?.Invoke(this, new EventArgs());
    }
}

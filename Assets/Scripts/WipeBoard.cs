﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WipeBoard : MonoBehaviour
{
    TextMesh t;
    public GameObject TaskOver;
    public GameObject NextTask;
    public GameObject[] BoardTexts;
    private bool allInActive;
    public GameObject randomize;

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "BoardText")
        {

            collision.gameObject.SetActive(false);
           /* //collision.gameObject.GetComponent<TextMesh>().color = new Color(1, 1, 1, 0.5f);
            t = collision.gameObject.GetComponent<TextMesh>();
            StartCoroutine("Fadeout");

            //  txt.SetActive(false);*/
        }
    }

    /* IEnumerator Fadeout()
     {
         for (float f = 1f; f >= -0.05f; f -= 0.05f)
            t.color = new Color(1, 1, 1, f);

         yield return new WaitForSeconds(0.05f);
     }
     */
    public void OnCollisionStay(Collision collision)
    {
        bool allInActive = true;

        for (int i = 0; i < BoardTexts.Length; i++)
        {
            if (BoardTexts[i].activeInHierarchy)
            {
                allInActive = false;
                break;
            }
        }

        if (allInActive)
            ChangeContent();
    }

    private void ChangeContent()
    {


        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "Module3")
        { 
             TaskOver.SetActive(false);
            NextTask.SetActive(true);
        }
        else
        randomize.GetComponent<RandomiseObjects>().Finish(7);
           
        
    }

    
}

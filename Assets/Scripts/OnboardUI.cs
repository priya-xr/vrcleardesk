﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRTK;

public class OnboardUI : MonoBehaviour
{
    public OnboardContent[] onboard;
    public Text head;
    public Text desc;
    public int currentindex = 0;
    private bool grabbed = false;
    public GameObject nextbutton;
    public GameObject CD;

    void Start()
    {
        head.text = onboard[currentindex].Heading.english;
        desc.text = onboard[currentindex].Description.english;
      
    }

    public void grabcheck()
    {
        grabbed = true;
        
    }
    public void ChangeUI()
    {
        if (currentindex < 3 && grabbed == false)
        {
            currentindex++;
            head.text = onboard[currentindex].Heading.english;
            desc.text = onboard[currentindex].Description.english;
        }

       

   
        if (grabbed == true && CD.GetComponent<VRTK_InteractableObject>().IsGrabbed() == true || CD.GetComponent<VRTK_InteractableObject>().IsInSnapDropZone()==true)
        {
            currentindex++;
            head.text = onboard[currentindex].Heading.english;
            desc.text = onboard[currentindex].Description.english;
        }

        if (currentindex == 3)
            nextbutton.SetActive(true);
    }




   [System.Serializable]
   public class OnboardContent
    {
        public Content Heading;
        public Content Description;
    }
}

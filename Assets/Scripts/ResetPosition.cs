﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPosition : MonoBehaviour
{
    MeshRenderer rend;
    Vector3 defaultPos;
    Quaternion defaultRot;
    
    // Start is called before the first frame update
    void Start()
    {
        //  rend = GetComponent<MeshRenderer>();
        defaultPos = transform.position;
        defaultRot = transform.rotation;
    }

   /* IEnumerator FadeOut()
    {
        for(float f= 1f; f>= -0.05f; f -= 0.05f)
        {
            Material m = rend.material;
            m.a = f;
            rend.material.color = m;
            yield return new WaitForSeconds(0.05f);
        }
    }

    public void startFading()
    {
        StartCoroutine("FadeOut");
    }
    */

    private void OnCollisionEnter(Collision collision)
    {
        // if (collision.gameObject.name == "BaseFloor")
        if (collision.gameObject.name != "CDSnapDropZone")
        {
            // startFading();
            transform.position = defaultPos;
            transform.rotation = defaultRot;
            Debug.Log("Object Faded");
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
